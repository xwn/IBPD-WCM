package com.ibpd.henuocms.common;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
/**
 * 自定义的日志打印类，抛弃了原先的功能较为强大的日志类引用，这个更好控制 
 * @author mg by qq:349070443
 *
 */
public class IbpdLogger {

	public static final Integer LOG_LEVEL_INFO=0;
	public static final Integer LOG_LEVEL_WARNING=1;
	public static final Integer LOG_LEVEL_DEBUG=2;
	public static final Integer LOG_LEVEL_ERROR=3;
	private Class clazz=null;
	private static Boolean isEnableDebug=null;
	public static IbpdLogger getLogger(Class clazz){
		IbpdLogger l=new IbpdLogger();
		l.setClazz(clazz);
		return l;
	}
	private IbpdLogger(){}
	public IbpdLogger(Class clazz){
		this.setClazz(clazz);
	}
	public Class getClazz() {
		return clazz;
	}
	public void setClazz(Class clazz) {
		this.clazz = clazz;
	}
	public void info(Object msg ,String ...args){
		printMsg(IbpdLogger.LOG_LEVEL_INFO,msg,args);
	}
	public void warning(Object msg ,String ...args){
		printMsg(IbpdLogger.LOG_LEVEL_WARNING,msg,args);
	}
	public void debug(Object msg ,String ...args){
		printMsg(IbpdLogger.LOG_LEVEL_DEBUG,msg,args);
	}
	public void error(Object msg ,String ...args){
		printMsg(IbpdLogger.LOG_LEVEL_ERROR,msg,args);
	}
	private Boolean checkDebugPrint(){
		if(isEnableDebug==null){
			String val=IbpdCommon.getInterface().getPropertiesValue("system.debug.enable");
			if(StringUtils.isBlank(val)){
				isEnableDebug=false;
				return false;
			}else{
				if(val.trim().toLowerCase().equals("true") || val.trim().toLowerCase().equals("1") || val.trim().toLowerCase().equals("y")){
					isEnableDebug=true;
					return true;
				}else{
					isEnableDebug=false;
					return false;
				}
			}
			
		}else{
			return isEnableDebug;
		}
	}
	private void printMsg(Integer type,Object msg,String ...args){
		if(!checkDebugPrint())
			return;
		String _type="";
		if(type.equals(LOG_LEVEL_INFO)){
			_type="信息";
		}else if(type.equals(LOG_LEVEL_WARNING)){
			_type="警告";
		}else if(type.equals(LOG_LEVEL_DEBUG)){
			_type="调试";
		}else if(type.equals(LOG_LEVEL_ERROR)){
			_type="错误";
		}
		StackTraceElement[] stacks = new Throwable().getStackTrace();       
        StringBuffer sb = new StringBuffer();    
	     sb.append("[");
	     if(stacks!=null && stacks.length>0){
//	         sb.append("fileName:" + stacks[0].getFileName())  
//	         .append(",className:" + stacks[0].getClassName())  
//	         .append(",methodName:" + stacks[0].getMethodName() + "()")  
//	         .append(",lineNumber:" + stacks[0].getLineNumber()) ;
	    	 sb.append(clazz.getName()).append(", at "+new Date().toLocaleString()+",");
	     }
	     sb.append(","+_type)
         .append("]");
    	 sb.append(msg);
    	 if(args==null || args.length==0)
    		 System.out.println(sb.toString());
    	 else{
    		 String _msg=sb.toString();
    		 for(Integer i=0;i<args.length;i++){
    			 _msg=_msg.replace("%"+(i+1), args[i]);
    		 }
    		 System.out.println(_msg);
    	 }
	}
	public static void main(String[] a){
		System.out.println();
		IbpdLogger log=IbpdLogger.getLogger(IbpdLogger.class);
		log.info("测试信息%1 at %2 at %3","第一个参数","第二个参数","第三个参数");
	}
}
