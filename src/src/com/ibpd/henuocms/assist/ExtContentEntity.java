package com.ibpd.henuocms.assist;

import java.lang.reflect.InvocationTargetException;

import com.ibpd.henuocms.entity.ContentAttrEntity;
import com.ibpd.henuocms.entity.ContentEntity;
import com.ibpd.henuocms.web.controller.manage.BaseController;
/**
 * 内容实体类的扩展类
 * @author mg
 *
 */
public class ExtContentEntity extends ContentEntity {

	private ContentAttrEntity attrEntity=null;
	public ExtContentEntity(ContentEntity c){
		BaseController controller=new BaseController();
		try {
			controller.swap(c, this);
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void setAttrEntity(ContentAttrEntity attrEntity) {
		this.attrEntity = attrEntity;
	}
	public ContentAttrEntity getAttrEntity() {
		return attrEntity;
	}
}
