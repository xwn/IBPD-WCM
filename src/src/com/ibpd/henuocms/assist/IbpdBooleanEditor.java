package com.ibpd.henuocms.assist;

import org.springframework.beans.propertyeditors.PropertiesEditor;
/**
 * spring的bollean类型数据处理器
 * @author mg by qq:349070443
 *
 */
public class IbpdBooleanEditor extends PropertiesEditor {
	   @Override  
	    public void setAsText(String text) throws IllegalArgumentException {  
	        if (text == null || text.equals("")) {  
	            text = "false";  
	        }else{
	        	if(text.equals("0") || text.trim().toLowerCase().equals("false")){
	        		text="false";
	        	}else if(text.equals("1")||text.trim().toLowerCase().equals("true")){
	        		text="true";
	        	}
	        }
	        setValue(Boolean.parseBoolean(text));  
	    }  
	  
	    @Override  
	    public String getAsText() {  
	        return getValue().toString();  
	    }  

}
