package com.ibpd.henuocms.web.controller.manage;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ibpd.common.EntityCommon;
import com.ibpd.common.PageCommon;
import com.ibpd.dao.impl.DaoImpl;
import com.ibpd.dao.impl.ServiceProxyFactory;
import com.ibpd.henuocms.entity.ValidateEntity;
import com.ibpd.henuocms.service.validate.IValidateService;
import com.ibpd.henuocms.service.validate.ValidateServiceImpl;

@Controller
public class Validate {
	@Resource(name="validateService")
	private IValidateService validateService;
	@RequestMapping("manage/validate/index.do")
	public String index(HttpServletResponse response) throws IOException{
		return "manage/validate/index";
	}	
	@RequestMapping("manage/validate/list.do")
	public void getList(HttpServletRequest req,HttpServletResponse resp,String order,Integer page,Integer rows,String sort,String queryData){
		order=(order==null)?"id":order;
		sort=(sort==null)?"asc":sort;
		page=(page==null)?1:page;
		rows=(rows==null)?10:rows;
		queryData=(queryData==null || queryData.equals(""))?null:queryData;
		validateService=(IValidateService) ((validateService==null)?ServiceProxyFactory.getServiceProxy(ValidateServiceImpl.class):validateService);
		validateService.setDao(new DaoImpl());
		String whereStr="";
		if(queryData!=null){
			whereStr=" where "+queryData;
		} 
		List<ValidateEntity> etList=validateService.getList("from "+validateService.getTableName()+whereStr,null,order+" "+sort,  rows,rows*(page-1));
		Long rowCount=validateService.getRowCount(whereStr,null);
		try {
		    JSONArray jsonArray = JSONArray.fromObject( etList );
		    
			PrintWriter out = resp.getWriter();
			 out.print("{\"total\":"+rowCount.toString()+",\"rows\":"+jsonArray+"}");
			 out.flush();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@RequestMapping("manage/validate/doEdit.do")
	public String doEdit(String validateName,Integer validateType,String dataType,String ajaxUrl,String rechek,String errormsg,String func,Long id) throws Exception{
		if(id!=null && id!=-1L){
			validateService=(IValidateService) ((validateService==null)?ServiceProxyFactory.getServiceProxy(ValidateServiceImpl.class):validateService);
			validateService.setDao(new DaoImpl());
			ValidateEntity ve=validateService.getEntityById(id);
			if(ve!=null){
				ve.setAjaxUrl(ajaxUrl);
				ve.setDataType(dataType);
				ve.setFunc(func);
//				ve.setOrder(Order);
				ve.setRechek(rechek);
				ve.setErrormsg(errormsg);
				ve.setValidateNamee(validateName);
				ve.setValidateType(validateType);
				validateService.saveEntity(ve);
				return "manage/validate/index";
			}else{
				throw new Exception("ID为"+id+"的validateEntity为空.");
			}

		}else{
			throw new Exception("validateEntity的ID为空.");
		}
	}
	@RequestMapping("manage/validate/doDel.do")
	public String doDel(String ids){
		validateService=(IValidateService) ((validateService==null)?ServiceProxyFactory.getServiceProxy(ValidateServiceImpl.class):validateService);
		validateService.setDao(new DaoImpl());
		validateService.batchDel(ids);
		return "manage/validate/index";
	}
	@RequestMapping("manage/validate/doAdd.do")
	public String doAdd(String validateName,Integer validateType,String dataType,String ajaxUrl,String rechek,String errormsg,String func) throws IOException{
		validateService=(IValidateService) ((validateService==null)?ServiceProxyFactory.getServiceProxy(ValidateServiceImpl.class):validateService);
		validateService.setDao(new DaoImpl());
		ValidateEntity ve=new ValidateEntity();
		ve.setAjaxUrl(ajaxUrl);
		ve.setCreateTime(new Date());
		ve.setDataType(dataType);
		ve.setFunc(func);
		ve.setOrder(0);
		ve.setRechek(rechek);
		ve.setErrormsg(errormsg);
		ve.setValidateNamee(validateName);
		ve.setValidateType(validateType);
		validateService.saveEntity(ve);
		return "manage/validate/index";
	}
}
