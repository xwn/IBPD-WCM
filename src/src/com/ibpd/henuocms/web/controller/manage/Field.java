package com.ibpd.henuocms.web.controller.manage;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ibpd.common.PageCommon;
import com.ibpd.dao.impl.DaoImpl;
import com.ibpd.dao.impl.ServiceProxyFactory;
import com.ibpd.henuocms.entity.FieldEntity;
import com.ibpd.henuocms.entity.ModelEntity;
import com.ibpd.henuocms.service.field.FieldServiceImpl;
import com.ibpd.henuocms.service.field.IFieldService;

@Controller
public class Field extends BaseController {
	private IFieldService fieldService=null;
	@RequestMapping("manage/field/index.do")
	public String index(Long modelId) throws IOException{
		PageCommon.printStringToConsole("modelId="+modelId);
		return "manage/field/index";
	}	
	@RequestMapping("manage/field/doAdd.do")
	public String doAdd(String displayName,String formName,Long modelId,Integer htmlType,Long validateModelId,Integer order,String defaultValue,String optionValue,Integer fieldType,String intro,String fieldLength) throws IOException{
		fieldService=(IFieldService) ((fieldService==null)?ServiceProxyFactory.getServiceProxy(FieldServiceImpl.class):fieldService);
		fieldService.setDao(new DaoImpl());
		FieldEntity field=new FieldEntity();
		field.setCreateTime(new Date());
		field.setDefaultValue(defaultValue);
		field.setDisplayName(displayName);
		field.setF_fieldType(fieldType);
		field.setFormName(formName);
		field.setHtmlType(htmlType);
		field.setIntro(intro);
		field.setModelId(modelId);
		field.setOptionValue(optionValue);
		field.setOrder(order);
		field.setUpdateTime(new Date());
		field.setValidateModelId(validateModelId);
		field.setFieldLength(fieldLength);
		
		fieldService.saveEntity(field);
		return "redirect:/manage/field/index.do?modelId="+modelId;
	}
	@RequestMapping("manage/field/doEdit.do")
	public String doEdit(Long id,String displayName,String formName,Integer htmlType,Long validateModelId,Integer order,String defaultValue,String optionValue,Integer fieldType,String intro,String fieldLength) throws IOException{
		fieldService=(IFieldService) ((fieldService==null)?ServiceProxyFactory.getServiceProxy(FieldServiceImpl.class):fieldService);
		fieldService.setDao(new DaoImpl());
		FieldEntity field=fieldService.getEntityById(id);
		if(field!=null){
			field.setDefaultValue(defaultValue);
			field.setDisplayName(displayName);
			field.setF_fieldType(fieldType);
			field.setFormName(formName);
			field.setHtmlType(htmlType);
			field.setIntro(intro);
			field.setOptionValue(optionValue);
			field.setOrder(order);
			field.setUpdateTime(new Date());
			field.setValidateModelId(validateModelId);
			field.setFieldLength(fieldLength);

			fieldService.saveEntity(field);	
		}else{
			//还没考虑好怎么捕获异常
			return super.ERROR_PAGE;
		}
		return "redirect:/manage/field/index.do?modelId="+field.getModelId().toString();
	}
	@RequestMapping("manage/field/doDel.do")
	public String doDel(String ids,Long modelId) throws IOException{
		fieldService=(IFieldService) ((fieldService==null)?ServiceProxyFactory.getServiceProxy(FieldServiceImpl.class):fieldService);
		fieldService.setDao(new DaoImpl());
		fieldService.batchDel(ids);
		return "redirect:/manage/field/index.do?modelId="+modelId;
	}
	@RequestMapping("manage/field/initDefaultField.do")
	public String initDefaultField(Long modelId) throws Exception{
		if(modelId==null)
			throw new Exception("modelId is null");
		fieldService=(IFieldService) ((fieldService==null)?ServiceProxyFactory.getServiceProxy(FieldServiceImpl.class):fieldService);
		fieldService.setDao(new DaoImpl());
		List<FieldEntity> fieldList=fieldService.getList("from "+fieldService.getTableName()+" where modelId="+modelId,null);
		String ids="";
		for(FieldEntity f:fieldList){
			ids+=f.getId()+",";
		}
		if(ids.length()>0){
			ids=ids.substring(0,ids.length()-1);
			fieldService.batchDel(ids);
		}
		//生成默认字段
		for(Integer i=1;i<=20;i++){
			FieldEntity field=new FieldEntity();
			field.setCreateTime(new Date());
			field.setDefaultValue(" ");
			field.setDisplayName("字段显示名称"+i);
			field.setF_fieldType(1);
			field.setFieldLength("255");
			field.setFieldType(1);
			field.setFormName("field"+i);
			field.setHtmlType(1);
			field.setIntro("字段的说明"+i);
			field.setModelId(modelId);
			field.setOptionValue(" ");
			field.setOrder(1);
			field.setUpdateTime(new Date());
			field.setValidateModelId(1L);
			fieldService.saveEntity(field);
			}
		return "redirect:/manage/field/index.do?modelId="+modelId;
	}
	@RequestMapping("manage/field/list.do")
	public void getList(HttpServletRequest req,HttpServletResponse resp,String modelId,String order,Integer page,Integer rows,String sort,String queryData){
		fieldService=(IFieldService) ((fieldService==null)?ServiceProxyFactory.getServiceProxy(FieldServiceImpl.class):fieldService);
		fieldService.setDao(new DaoImpl());
		super.getList(req, fieldService, resp, order, page, rows, sort, "modelId="+modelId);
	}
}
