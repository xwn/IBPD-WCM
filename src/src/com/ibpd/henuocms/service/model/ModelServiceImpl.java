package com.ibpd.henuocms.service.model;

import org.springframework.stereotype.Service;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.henuocms.entity.ModelEntity;
@Service("modelService")
public class ModelServiceImpl extends BaseServiceImpl<ModelEntity> implements IModelService {
	public ModelServiceImpl(){
		super();
		this.tableName="ModelEntity";
		this.currentClass=ModelEntity.class;
		this.initOK();
	}
	
}
 