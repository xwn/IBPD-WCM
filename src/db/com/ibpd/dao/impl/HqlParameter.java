package com.ibpd.dao.impl;

public class HqlParameter {
	private String paramName;
	private Object paramValue;
	public HqlParameter(){
		super();
	}
	public HqlParameter(String paramName,Object paramValue,DataType_Enum dataType){
		this.paramName=paramName;
		this.paramValue=paramValue;
		this.dataType=dataType;
	}
	public enum DataType_Enum{
		String(),
		Integer(),
		Date(),
		TimeSamper(),
		Long(),
		Boolean()
	}
	private DataType_Enum dataType;
	public String getParamName() {
		return paramName;
	}
	public void setParamName(String paramName) {
		this.paramName = paramName;
	}
	public Object getParamValue() {
		return paramValue;
	}
	public void setParamValue(Object paramValue) {
		this.paramValue = paramValue;
	}
	public DataType_Enum getDataType() {
		return dataType;
	}
	public void setDataType(DataType_Enum dataType) {
		this.dataType = dataType;
	}
	
}
