package com.ibpd.shopping.service.spec;

import java.util.List;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.shopping.entity.SpecEntity;

public interface ISpecService extends IBaseService<SpecEntity> {
	List<SpecEntity> getProductSpecList(Long productId);
}
