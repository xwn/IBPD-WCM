package com.ibpd.shopping.service.product;

import java.util.List;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.shopping.entity.ProductAttrDefineEntity;

public interface IProductAttrDefineService extends IBaseService<ProductAttrDefineEntity> {
	List<ProductAttrDefineEntity> getListByCatalogId(Long id,Integer type);
	List<ProductAttrDefineEntity> getParamListByCatalogId(Long id);
	List<ProductAttrDefineEntity> getAttributeListByCatalogId(Long id);
	ProductAttrDefineEntity getParentAttribute(Long pid);

}
