package com.ibpd.shopping.service.cart;

import java.util.List;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.shopping.entity.CartEntity;

public interface ICartService extends IBaseService<CartEntity> {
	List<CartEntity> getCartProductListByUserId(Long userId);

	void clearCart(Long currentLoginedUserId);
}
