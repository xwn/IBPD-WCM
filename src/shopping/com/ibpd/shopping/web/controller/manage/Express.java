package com.ibpd.shopping.web.controller.manage;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jsoup.helper.StringUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ibpd.henuocms.common.IbpdLogger;
import com.ibpd.henuocms.common.SysUtil;
import com.ibpd.henuocms.web.controller.manage.BaseController;
import com.ibpd.shopping.entity.ExpressEntity;
import com.ibpd.shopping.entity.TenantEntity;
import com.ibpd.shopping.service.express.ExpressServiceImpl;
import com.ibpd.shopping.service.express.IExpressService;
import com.ibpd.shopping.service.tenant.ITenantService;
import com.ibpd.shopping.service.tenant.TenantServiceImpl;

@Controller
public class Express extends BaseController {
	@RequestMapping("Manage/Exp/index.do")
	public String index(Model model,HttpServletRequest req) throws IOException{
		model.addAttribute("pageTitle","快递方式管理");
		return "manage/s_exp/index";
	}
	@RequestMapping("Manage/Exp/list.do")
	public void list(HttpServletResponse resp,String order,Integer page,Integer rows,String sort,String queryString,HttpServletRequest req) throws IOException{
		IExpressService tenantServ=(IExpressService) getService(ExpressServiceImpl.class);
		Long tenantId=SysUtil.getCurrentTenantId(req.getSession());
		String query="";
		if(tenantId==null || tenantId<=0){
			if(SysUtil.getCurrentLoginedUserType(req.getSession()).trim().toLowerCase().equals("tenant"))
				query="";//可以管理所有的
			else
				query="tenantId=-99";//登陆方式不对，不能查看
		}else{
			query="tenantId="+tenantId;
		}
//		tenantServ.getDao().clearCache();
		super.getList(req, tenantServ, resp, order, page, rows, sort, query);
	}
	@RequestMapping("Manage/Exp/props.do")
	public String props(Long id,Model model) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		if(id==null || id<0L){
			model.addAttribute("errorMsg","参数错误");
			return super.ERROR_PAGE;
		}
		IExpressService tenantServ=(IExpressService) getService(ExpressServiceImpl.class);
		ExpressEntity ce=tenantServ.getEntityById(id);
		if(ce==null){
			model.addAttribute("errorMsg","没有该类别");
			return super.ERROR_PAGE;
		}
		model.addAttribute("pageTitle","参数设置");
		model.addAttribute("htmls",super.getHtmlString(ce, "express.edit.field"));
		model.addAttribute("entity",ce);
		return "manage/s_exp/props";
	}	
	@RequestMapping("Manage/Exp/saveProp.do")
	public void saveProps(Long id,String field,String value,HttpServletResponse resp) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IOException{
		if(id!=null && id!=-1){
			IExpressService tenantServ=(IExpressService) getService(ExpressServiceImpl.class);
			ExpressEntity cata=tenantServ.getEntityById(id);
				if(cata!=null){
					//先确定参数类型
					Method tmpMethod=cata.getClass().getMethod("get"+field.substring(0,1).toUpperCase()+field.substring(1), new Class[]{});
					
					
					Method method=cata.getClass().getMethod("set"+field.substring(0,1).toUpperCase()+field.substring(1), new Class[]{tmpMethod.getReturnType()});
					Object val=getValue(value,tmpMethod.getReturnType().getSimpleName());
					IbpdLogger.getLogger(Express.class).info("value="+val);
					method.invoke(cata, val);
					tenantServ.saveEntity(cata);
//					tenantServ.getDao().clearCache();
					super.printMsg(resp, "99", cata.getId().toString(), "保存成功");		
				}else{
					super.printMsg(resp, "-1", "", "没有该类别");
				}
		}else{
			super.printMsg(resp, "-2", "", "参数错误");			
		}
	}
	@RequestMapping("Manage/Exp/toExempt.do")
	public String toExempt(Model model,HttpServletResponse resp) throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{

		model.addAttribute("pageTitle","设定基准值");
		return "manage/s_exp/exempt";
	}
	@RequestMapping("Manage/Exp/doExempt.do")
	public void doExempt(Model model,HttpServletResponse resp,HttpServletRequest req,Double price) throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		IExpressService expServ=(IExpressService) getService(ExpressServiceImpl.class);
		Long tenantId=SysUtil.getCurrentTenantId(req.getSession());
		tenantId=(tenantId==null)?-1L:tenantId;
		if(tenantId<=0){
			super.printMsg(resp, "-1", "-1", "未找到商户信息");
		}
		List<ExpressEntity> expList=expServ.getList("from "+expServ.getTableName()+" where tenantId="+tenantId,null);
		if(expList!=null){
			for(ExpressEntity exp : expList){
				exp.setExempt(price);
				expServ.saveEntity(exp);
			}
		}
		super.printMsg(resp, "1", "1", "执行成功");
	}
	@RequestMapping("Manage/Exp/toAdd.do")
	public String toAdd(Model model,HttpServletResponse resp) throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{

		model.addAttribute("pageTitle","添加");
		model.addAttribute("htmls",super.getHtmlString(null, "express.add.field"));
		return "manage/s_exp/add";
	}
	@RequestMapping("Manage/Exp/toEdit.do")
	public String toEdit(Long id,Model model,HttpServletResponse resp) throws IOException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		if(id==null){
			return this.ERROR_PAGE;
		}
		IExpressService tenantServ=(IExpressService) getService(ExpressServiceImpl.class);
		ExpressEntity cata=tenantServ.getEntityById(id);
		if(cata==null){
			model.addAttribute("errorMsg","没有该类别");
			return this.ERROR_PAGE;
		}
		model.addAttribute("pageTitle","编辑");
		model.addAttribute("htmls",super.getHtmlString(cata, "express.edit.field"));
		model.addAttribute("entity",cata);
		return "manage/s_exp/edit";
	}
	@RequestMapping("Manage/Exp/doEdit.do")
	public void doEdit(ExpressEntity entity,HttpServletResponse resp,HttpServletRequest req) throws IOException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		if(entity!=null && entity.getId()!=null && entity.getId()!=-1L){
			
			IExpressService tenantServ=(IExpressService) getService(ExpressServiceImpl.class);
			ExpressEntity cata=tenantServ.getEntityById(entity.getId());
			if(cata==null){
				super.printMsg(resp, "-1", "", "没有该快递方式");
				return;
			}else{
				swap(entity,cata);
				cata.setTenantId(SysUtil.getCurrentTenantId(req.getSession()));
				tenantServ.saveEntity(cata);
				super.printMsg(resp, "99", cata.getId().toString(), "保存成功");
				return;
			}
		}else{
			super.printMsg(resp, "-2", "", "参数错误");
			return;
		}
		
	}
	@RequestMapping("Manage/Exp/doAdd.do")
	public void doAdd(ExpressEntity entity,HttpServletResponse resp,HttpServletRequest req) throws IOException{
		
		
		IExpressService tenantServ=(IExpressService) getService(ExpressServiceImpl.class);
		entity.setTenantId(SysUtil.getCurrentTenantId(req.getSession()));
		tenantServ.saveEntity(entity);	
		super.printMsg(resp, "99", "-1", "保存成功");
		 
	}
	@RequestMapping("Manage/Exp/doDel.do")
	public void doDel(String ids,HttpServletResponse resp) throws IOException{
		if(StringUtil.isBlank(ids)){
			super.printMsg(resp, "-1", "", "没有选择项");	
		}
		IExpressService cataServ=(IExpressService) getService(ExpressServiceImpl.class);
		cataServ.batchDel(ids);
		super.printMsg(resp, "99", "", "操作成功");	
	}
}