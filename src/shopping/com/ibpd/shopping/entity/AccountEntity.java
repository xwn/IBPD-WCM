
package com.ibpd.shopping.entity;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ibpd.entity.baseEntity.IBaseEntity;

@Entity
@Table(name="t_s_account")
public class AccountEntity extends IBaseEntity{
	
	private static final long serialVersionUID = 1L;

	public static String RESSZ_PASSED="y";
	
	@Id @Column(name="f_id",nullable=true) @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(name="f_account",length=45,nullable=true)
	private String account=" ";
	
	@Column(name="f_nickname",length=45,nullable=true)
	private String nickname=" ";
	
	@Column(name="f_logo",length=150,nullable=true)
	private String logo="noimage.png";
	
	@Column(name="f_password",length=45,nullable=true)
	private String password=" ";
	
	@Column(name="f_city",length=45,nullable=true)
	private String city=" ";
	
	@Column(name="f_address",length=45,nullable=true)
	private String address=" ";
	
	@Column(name="f_postcode",length=45,nullable=true)
	private String postcode=" ";
	
	@Column(name="f_cardType",length=45,nullable=true)
	private String cardType=" ";
	
	@Column(name="f_cardNO",length=45,nullable=true)
	private String cardNO=" ";

	@Column(name="f_grade",nullable=true)
	private Integer grade=1;
	
	@Column(name="f_amount",length=45,nullable=true)
	private String amount=" ";
	
	@Column(name="f_tel",length=45,nullable=true)
	private String tel=" ";
	
	@Column(name="f_email",length=45,nullable=true)
	private String email=" ";
	
	@Column(name="f_emailIsActive",length=2,nullable=true)
	private String emailIsActive="n";
	
	@Column(name="f_freeze",length=2,nullable=true)
	private String freeze="n";
	
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_lastLoginTime",nullable=true)
	private Date lastLoginTime=new Date();
	
	@Column(name="f_lastLoginIp",length=45,nullable=true)
	private String lastLoginIp="";
	
	@Column(name="f_lastLoginArea",length=25,nullable=true)
	private String lastLoginArea="";
	
	@Column(name="f_diffAreaLogin",length=1,nullable=true)
	private String diffAreaLogin="n";
	
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_regeistDate",nullable=true)
	private Date regeistDate=new Date();
	
	@Column(name="f_freezeStartdate",nullable=true)
	private Date freezeStartdate=new Date();
	
	@Column(name="f_freezeEnddate",nullable=true)
	private Date freezeEnddate=new Date();

	@Column(name="f_openId",length=45,nullable=true)
	private String openId=null;

	@Column(name="f_accessToken",length=45,nullable=true)
	private String accessToken=" ";

	@Column(name="f_alipayUseId",length=45,nullable=true)
	private String alipayUseId=null;

	@Column(name="f_sinaWeiboID",length=45,nullable=true)
	private String sinaWeiboID=null;

	@Column(name="f_sex",length=2,nullable=true)
	private String sex=null;

	@Column(name="f_trueName",length=45,nullable=true)
	private String trueName=null;
	
	@Column(name="f_birthday",nullable=true)
	private Date birthday=new Date();

	@Column(name="f_province",length=45,nullable=true)
	private String province=null;

	@Column(name="f_accountType",length=25,nullable=true)
	private String accountType=null;

	@Column(name="f_rank",length=25,nullable=true)
	private String rank="R1";

	@Column(name="f_score",nullable=true)
	private Integer score=0;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getCardNO() {
		return cardNO;
	}

	public void setCardNO(String cardNO) {
		this.cardNO = cardNO;
	}

	public Integer getGrade() {
		return grade;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmailIsActive() {
		return emailIsActive;
	}

	public void setEmailIsActive(String emailIsActive) {
		this.emailIsActive = emailIsActive;
	}

	public String getFreeze() {
		return freeze;
	}

	public void setFreeze(String freeze) {
		this.freeze = freeze;
	}

	public Date getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public String getLastLoginIp() {
		return lastLoginIp;
	}

	public void setLastLoginIp(String lastLoginIp) {
		this.lastLoginIp = lastLoginIp;
	}

	public String getLastLoginArea() {
		return lastLoginArea;
	}

	public void setLastLoginArea(String lastLoginArea) {
		this.lastLoginArea = lastLoginArea;
	}

	public String getDiffAreaLogin() {
		return diffAreaLogin;
	}

	public void setDiffAreaLogin(String diffAreaLogin) {
		this.diffAreaLogin = diffAreaLogin;
	}

	public Date getRegeistDate() {
		return regeistDate;
	}

	public void setRegeistDate(Date regeistDate) {
		this.regeistDate = regeistDate;
	}

	public Date getFreezeStartdate() {
		return freezeStartdate;
	}

	public void setFreezeStartdate(Date freezeStartdate) {
		this.freezeStartdate = freezeStartdate;
	}

	public Date getFreezeEnddate() {
		return freezeEnddate;
	}

	public void setFreezeEnddate(Date freezeEnddate) {
		this.freezeEnddate = freezeEnddate;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getAlipayUseId() {
		return alipayUseId;
	}

	public void setAlipayUseId(String alipayUseId) {
		this.alipayUseId = alipayUseId;
	}

	public String getSinaWeiboID() {
		return sinaWeiboID;
	}

	public void setSinaWeiboID(String sinaWeiboID) {
		this.sinaWeiboID = sinaWeiboID;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getTrueName() {
		return trueName;
	}

	public void setTrueName(String trueName) {
		this.trueName = trueName;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getLogo() {
		return logo;
	}
}
